import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('counter', () => {
  const snackbar = ref(false)
  const text = ref('')
  const showMassage = function (msg: string) {
    text.value = msg
    snackbar.value = true
  }
  return { showMassage, snackbar, text }
})
